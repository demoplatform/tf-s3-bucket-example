provider "aws" {
  profile = "default"
  region  = "eu-west-2"
}

resource "aws_s3_bucket" "b1" {
  bucket = var.bucket_name
  acl    = "private"
}
